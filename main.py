from fastapi import FastAPI
import redis
from pydantic import BaseModel
from controller.fshare import Fshare
import pickle
import uvicorn
from dotenv import load_dotenv
import threading
import os
import time
import requests

last_change_ip = 0


def change_ip():
    API_KEY = os.getenv('API_KEY')
    TIME_DELAY = int(os.getenv('TIME_DELAY'))
    global last_change_ip
    if time.time() - last_change_ip > 120:
        requests.get(url='http://proxyxoay.net/api/rotating-proxy/change-key-ip/' + API_KEY)
        last_change_ip = time.time()
        time.sleep(10)



load_dotenv()
app = FastAPI()

redis_pool = redis.ConnectionPool(host='localhost', port=6379)
redis_connect = redis.Redis(connection_pool=redis_pool)

@app.on_event("shutdown")
def shutdown():
    redis_connect.close()



class Account(BaseModel):
    email: str
    password: str

class Tranfer(BaseModel):
    account: Account
    email_receive: str
    number: str
    message: str
    level2password: str 
    oldPoint: int


@app.post("/tranfer")
def tranferOldPoint(tranfer: Tranfer):
    change_ip()
    IP_PROXY = os.getenv('IP_PROXY')
    fshare = Fshare(email=tranfer.account.email, password=tranfer.account.password, proxy=IP_PROXY)
    """
    * Check had cookie from before login, if dont, login using password
    """
    if not redis_connect.exists(tranfer.account.email):
        login_status = fshare.login()
        if login_status['code'] == 400:
            return {'error': 'Login fail'}
    else:
        """
        * If had cookie from before login, check cookie live or die, if die, login using email password
        """
        fshare.updateCookie(pickle.loads(redis_connect.get(tranfer.account.email)))
        if not fshare.checkLogin():
            redis_connect.delete(tranfer.account.email)
            login_status = fshare.login()
            if login_status['code'] == 400:
                return {'error': 'Login fail'}
    fshare.send_level_2_password(tranfer.level2password)
    if tranfer.oldPoint == 0:
        tranfer_status = fshare.tranferNewPoint(email=tranfer.email_receive, number=tranfer.number, levelTwoPassword=tranfer.account.password, message=tranfer.message)
    else:
        tranfer_status = fshare.tranferOldPoint(email=tranfer.email_receive, number=tranfer.number, levelTwoPassword=tranfer.account.password, message=tranfer.message)
    redis_connect.set(tranfer.account.email, pickle.dumps(fshare.getCookie()))
    return 0    
    

class UpgradeVip(BaseModel):
    account: Account
    vipDay: str

@app.post('/upgrade')
def upgrade(upgrade: UpgradeVip):
    change_ip()
    IP_PROXY = os.getenv('IP_PROXY')
    fshare = Fshare(email=upgrade.account.email, password=upgrade.account.password, proxy=IP_PROXY)
    """
    * Check had cookie from before login, if dont, login using password
    """
    if not redis_connect.exists(upgrade.account.email):
        login_status = fshare.login()
        if login_status['code'] == 400:
            return {'error': 'Login fail'}
    else:
        """
        * If had cookie from before login, check cookie live or die, if die, login using email password
        """
        fshare.updateCookie(pickle.loads(redis_connect.get(upgrade.account.email)))
        if not fshare.checkLogin():
            redis_connect.delete(upgrade.account.email)
            login_status = fshare.login()
            if login_status['code'] == 400:
                return {'error': 'Login fail'}
    status = fshare.upgradeVipOldPoint(upgrade.vipDay)
    redis_connect.set(upgrade.account.email, pickle.dumps(fshare.getCookie()))
    return status


@app.get('/quote')
def get_quote(id: str = ''):
    if id == '':
        return {'error': 'id must be not null or empty'}
    change_ip()
    IP_PROXY = os.getenv('IP_PROXY')
    # print('https://www.fshare.vn/api/v3/tools/get-download-traffic-info?user_id={id_code}&verifyCode=cFWZJ%3Dy%21NK%40-k?63M%2AEy%2BL'.format(id_code = id))
    # return 0
    return requests.get('https://www.fshare.vn/api/v3/tools/get-download-traffic-info?user_id={id_code}&verifyCode=cFWZJ%3Dy%21NK%40-k?63M%2AEy%2BL'.format(id_code = id), proxies={
        'http': 'http://' + IP_PROXY,
        'https': 'http://' + IP_PROXY,
    }).json()

@app.get('/test')
def test():
    change_ip()
    IP_PROXY = os.getenv('IP_PROXY')
    print(IP_PROXY)
    return requests.get('https://api.ipify.org', proxies={
        'http': 'http://' + IP_PROXY,
        'https': 'http://' + IP_PROXY,
    }).text